/**
	Function for drawing a Symbol Table to standard out.
	Output can be changed by using output redirection.
	
	The title of the Symbol Table should be drawn to standard out before this ]
	function is called.
	
	(c) Adam Docherty, November 2013
*/

#include <stdlib.h>

#include "SymbolTable.h"
#include "SymbolTableDrawer.h"

int drawSymbolTable(symbolNode *topNode, int type)
{
	symbolNode *node = topNode;
	if (node == NULL)  {
		printf("Empty");
	}
	else {
		while (node!=NULL) {
			if(type) printf("%s\t%c\n", node->name, node->value);
			else	 printf("%s\t%d\n", node->name, node->value);
			node = node->next;
		}
	}
	
	printf("\n\n");
	return 0;
}
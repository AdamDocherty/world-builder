#ifndef PARSE_TREE_INTERPRETER_H
#define PARSE_TREE_INTERPRETER_H

#include "ParseTree.h"

void interpret (treeNode* levelDefinitionList);

#endif//! PARSE_TREE_INTERPRETER_H
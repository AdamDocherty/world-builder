# World Builder
## Introduction
World Builder is a tool for generating 2D ASCII maps.

### Purpose
World Builder can be used to create tile based maps to be used by other applications such as games.


## Example
### Example Scripts
```
beginlevel

name="MyLevel"
size=(10, 10)

define water w
define tree T

var i = 2

repeat 5 times

draw tree (i, 4)

i = i + 2

endrepeat

draw water circle {
	centre=(8, 6),
	r=2
}

draw water line {
	from=(8, 6),
	to=(3, 1)
}

endlevel

```

### Example Output

```
MyLevel
100				/* This number is the size of the level string in Bytes */

..w.......
...w......
....w.....
.T.T.w.T.T
......ww..
......www.
.......w..
..........
..........
..........
```
Note that the water drawn in the `draw water line` portion of the program overwrote one of the trees that was created at the start.

##Feedback

>Student: ADAM DOCHERTY (1105555)
 
>Overall grade: A 18
 
>The comments below are feedback on the areas of your report and submission that were identified in the coursework specification as contributing to the overall grade.
 
>=== Tool Manual - grade A
Relevant and useful context. Language is complex and appropriate. Watch consistency; BNF is a formal notation and should be correct but you use <Coordinates> and <Coordinate> for the same thing. Terminal symbols should be separated from the main BNF rules. I wonder how you are going to differentiate a <Character> from an <identifier> name? Appropriate comments are included to describe the semantics. Good user manual. I like the option to use flags for the processor, production strenght programming here.
 
>=== Critique - grade B
Relevant and appropriate identification of limitations but you could have signposted how these might be overcome in a future version. A rather limited section, you should also have talked about how useful your tool can be; 'critique' does not mean just pointing out the bad bits, it also means identifying the good bits. Issues raised do however indicate insight on your part.
 
>=== Implementation - grade A
Code description is appropriate although perhaps a little thin. implementation is very good showing sound programming practices.
 
>=== Additional comments
In our consultations in the lab you appeared to have considered all the issues I raised with you regarding pipeline, fixed or dynamic tile representations, language flexibility, etc. Excellent preparation that resulted in an excellent coursework.

The report I wrote for this program can be found [here](https://www.dropbox.com/s/hp3xq8natmdw79n/AG0900A-report.pdf).
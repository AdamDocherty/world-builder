/*
	Data structure for
*/

#include "Level.h"

Level* newLevel()
{
	Level *level = (Level*)malloc (sizeof(Level));
	memset ((void*)level, 0, sizeof(Level));
	return level;
}

void createTiles(Level* level, size_t x, size_t y)
{
	char* tiles = (char*)malloc (x * y);
	memset ((void*)tiles, '.', (x * y) + 1);
	tiles[x*y] = 0; /* Make sure the tile block is null-terminated */
	level->tiles = tiles;
	level->width = x;
	level->height = y;
	level->size = x*y;
}

void freeLevel(Level* level)
{
	free(level->tiles);
	free(level);
}

LevelListNode* newLevelListNode()
{
	LevelListNode* node = (LevelListNode*)malloc (sizeof(LevelListNode));
	memset ((void*)node, 0, sizeof(LevelListNode));
	return node;
}
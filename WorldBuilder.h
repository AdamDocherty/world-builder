/*
	Header file for the World Builder application.
*/

#ifndef WORLD_BUILDER_H
#define WORLD_BUILDER_H

/* 	Definitions for flags which are used to set the 'flags' variable 
	Must not exceed (1 << sizeof (int) - 1) */

#define FLAG_HELP 1
#define FLAG_DRAW_PARSE_TREE 2
#define FLAG_DRAW_SYMBOL_TABLE 4
#define FLAG_PRINT_WARNINGS 8
#define FLAG_OUTPUT_XML 16
#define FLAG_CANCEL_INTERPRETER 32

#endif//! WORLD_BUILDER_H
#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

/* A single entry in the symbol table. */
typedef struct symbolNode {
	char *name;	/* the identifier name. */
	int value;	/* value assigned to it. */
	struct symbolNode *next;	/* pointer to next entry in the list. */
} symbolNode;

/* returns 1 if there is an entry in the table for the specidied id name. 0 otherwise */
int isDeclared (symbolNode* symbolTable, char* id);
/*  adds a new entry for the specified id name, returns 0 if already in the table */
int addId(symbolNode** symbolTable, char* id);
/* sets the v parameter to the value associated in the table with the specified id name; returns 0 if no table entry exists */
int getValue (symbolNode* symbolTable, char *id, int *v);
/* sets the value associated with the specified id name to v; returns 0 if no table entry exists */
int setValue (symbolNode* symbolTable, char *id, int v);

#endif//! SYMBOL_TABLE_H
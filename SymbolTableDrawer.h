#ifndef SYMBOL_TABLE_DRAWER_H
#define SYMBOL_TABLE_DRAWER_H

#include "SymbolTable.h"

//! Draws a symbol table
//! @param	type	The type of symbol, 0 for int, anything else for char
int drawSymbolTable(symbolNode *topNode, int type);

#endif//! SYMBOL_TABLE_DRAWER_H
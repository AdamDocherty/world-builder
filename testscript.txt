/* First example in the Manual */
beginlevel

name="MyLevel"
size=(10, 10)

define water w
define tree T

var i

repeat 5 times

draw tree (i, 4)

i = i + 2

endrepeat

draw water circle {
	centre=(8, 6),
	radius=2
}

draw water line {
	from=(8, 6),
	to=(3, 1)
}

draw tree rect {
	from=(4, 4),
	to=(2, 2)
}

endlevel

/* Second example in the manual */
beginlevel

name = "Dundee"
size = (60, 25)

define road r
define abertay A
define library L
define librarySecondWing L /* change L to seperate the first and second wings */

draw librarySecondWing rect {
	from = (22, 12),
	to = (4, 4)
}

draw library circle {
	centre = (22, 12),
	radius = 8
}

draw road line {
	from = (0, 20),
	to = (59, 20)
}

var i = 30
var j

/* Draw the abertay building */
repeat 30 times
	// 30 units wide
	j = 0
	repeat 20 times
		// 20 units tall
		draw abertay (i, j)
		j = j + 1
	endrepeat
	i = i + 1
endrepeat

draw road line {
	from = (29, 0),
	to = (29, 25)
}

endlevel

/* Bonus Example */
beginlevel

name="Your Level"
size=(13, 15)

define water w
define tree T
define sand s

var i = 2

repeat 5 times

draw tree (i, i + 4)

i = i + 1

endrepeat

draw sand circle {
	centre=(8, 6),
	radius=3
}

draw water line {
	from=(8, 6),
	to=(3, 1)
}

endlevel
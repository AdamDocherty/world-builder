#include "SymbolTable.h"

/* Data structure to hold information for a single level */
typedef struct Level {
	char* name; /* The name of the level */
	size_t size; /* The size of the tiles array */
	char* tiles; /* Array to store tiles */
	int width, height; /* The width and height of the level. */
	symbolNode *variableTable;
	symbolNode *symbolTable;
} Level;

Level* newLevel(); /* Allocates memory for a new level */
void createTiles(Level* level, size_t x, size_t y); /* Allocates memory for the tiles in a level */
void freeLevel(Level* level); /* Frees the memory of a level */

/* Data structure to hold list of all generated levels */
typedef struct LevelListNode {
	Level* this;
	struct LevelListNode* next;
} LevelListNode;

LevelListNode* newLevelListNode(); /* Allocate memory for level list node */
/*
	The interpreter functions for the WorldBuilder compiler.
	(c) Adam Docherty, November 2013
	
	Note:
		The output is written to outputFile, setup in main.c; 
		may be a file or stdout
	
	Only the interpret () function is exposed to the application.
	The other functions defined here are internal for interpreting each
	different types of parse tree branches and will be called appropriately 
	as the tree is traversed 
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h> /* Required for the drawing functions */

#include "WorldBuilder.h"

#include "ParseTree.h"
#include "SymbolTable.h"
#include "SymbolTableDrawer.h"
#include "Level.h"

#include "ParseTreeInterpreter.h"

#define sign(x) ((x > 0)? 1 : ((x < 0)? -1: 0))

extern FILE *outputFile;
extern int errorCount;

/* Store the program flags */
extern int flags;

/* Draw all the levels stored in the level list */
void outputPlainText(LevelListNode* levelList);
void outputXML(LevelListNode* levelList);

/* Functions for interpreting the parse Tree */
void interpretLevelDefinition		(Level*, treeNode*);
void interpretLevelInfo				(Level*, treeNode*);
void interpretSymbolDefinitionList	(Level*, treeNode*);
void interpretSymbolDefinition		(Level*, treeNode*);
void interpretStatementList			(Level*, treeNode*);
void interpretStatement				(Level*, treeNode*);
void interpretRepeatBlock			(Level*, treeNode*);
void interpretVariableDeclaration	(Level*, treeNode*);
void interpretAssignment			(Level*, treeNode*);
void interpretDrawCommand			(Level*, treeNode*);
void interpretDrawPoint				(Level*, treeNode*);
void interpretDrawCircle			(Level*, treeNode*);
void interpretDrawLine				(Level*, treeNode*);
void interpretDrawRect				(Level*, treeNode*);

int evaluate 						(Level*, treeNode*);

/* Functions for drawing the tiles */
void plotSymbol(Level*, char, int, int);

void interpret (treeNode* levelDefinitionList)
{
	LevelListNode* levelList = newLevelListNode(); /* First element in the level list */
	LevelListNode* levelListIter = levelList; /* Last node of the level list */
	Level* level = newLevel();
	
	treeNode* node;
	node = levelDefinitionList->structure;
	while (node->type == NTLevelDefinitionList) {
		Level* nlevel = newLevel();
		interpretLevelDefinition(nlevel, node->next);
		node = node->structure;
		
		/* create a new level list node and set the iterator to the back of the list */
		levelListIter->this = nlevel;
		levelListIter->next = newLevelListNode();
		levelListIter = levelListIter->next; 
	}
	/* at this point, node will be pointing to an NTLevelDefinition  */
	interpretLevelDefinition(level, node);
	levelListIter->this = level;
	
	if (errorCount == 0) {
		if (flags & FLAG_OUTPUT_XML)
			outputXML(levelList);
		else
			outputPlainText(levelList);
	}
	
	/* clean up */
	for (levelListIter = levelList; levelListIter != NULL; levelListIter = levelListIter->next) {
		freeLevel(levelListIter->this);
	}
}

void interpretLevelDefinition(Level* level, treeNode* node)
{
	treeNode* levelInfo = node->structure;
	treeNode* symbolDefList = levelInfo->next;
	treeNode* statementList = symbolDefList->next;
	
	interpretLevelInfo(level, levelInfo);
	interpretSymbolDefinitionList(level, symbolDefList);
	interpretStatementList(level, statementList);
}

void interpretLevelInfo(Level* level, treeNode* levelInfo)
{
	treeNode* name = levelInfo->structure;
	treeNode* size = name->next;
	/* size->structure->structure is the coordinate tree node */
	int x = size->structure->structure->structure->value.intValue;
	int y = size->structure->structure->next->structure->value.intValue;
	
	level->name = name->value.strValue;
	
	if (x < 1) {
		fprintf (stderr, "Error: %s level width set to %d, which is invalid", level->name, x);
		errorCount++;
		return;
	}
	if (y < 1) {
		fprintf (stderr, "Error: %s level height set to %d, which is invalid", level->name, y);
		errorCount++;
		return;
	}
	
	/* Allocate memory for the tile block */
	createTiles(level, x, y);
}

void interpretSymbolDefinitionList(Level* level, treeNode* node)
{	
	if (node->structure->type == NTSymbolDefinitionList) {
		interpretSymbolDefinitionList(level, node->structure);
		interpretSymbolDefinition(level, node->structure->next);
	}
	else {
		interpretSymbolDefinition(level, node->structure);
	}
}

void interpretSymbolDefinition(Level* level, treeNode* node)
{
	if (!isDeclared (level->symbolTable, node->structure->value.strValue))
	{
		addId(&level->symbolTable, node->structure->value.strValue);
	}
	else
	{
		if (flags & FLAG_PRINT_WARNINGS) 
			fprintf (stderr, "Warning: Redefinition of symbol %s to %c in level %s.\n", 
			node->structure->value.strValue, node->value.charValue, level->name);
	}
	setValue(level->symbolTable, node->structure->value.strValue, node->value.charValue);
}

void interpretStatementList(Level* level, treeNode* node)
{
	if(node->structure->type == NTStatementList) {
		interpretStatementList(level, node->structure);
		interpretStatement(level, node->structure->next);
	} else {
		interpretStatement(level, node->structure);
	}
}

void interpretStatement(Level* level, treeNode* node)
{
	switch(node->structure->type) {
		case NTRepeatBlock: interpretRepeatBlock(level, node->structure); break;
		case NTVariableDeclaration: interpretVariableDeclaration(level, node->structure); break;
		case NTAssignment: interpretAssignment(level, node->structure); break;
		case NTDrawCommand: interpretDrawCommand(level, node->structure); break;
		default: ;
			errorCount++;
			fprintf(stderr, "Compiler Error: Type %d is not a valid statement type.", node->structure->type);
	}
}

void interpretRepeatBlock(Level* level, treeNode* node)
{
	int specifier, loop;
	specifier = evaluate (level, node->structure);
	for (loop=0; loop<specifier; loop++) {
		interpretStatementList (level, node->structure->next);
	}
}

int evaluate(Level* level, treeNode *expression)
{
	treeNode *node = expression->structure;
	int result, value1, value2;
	switch (node->type) {
	case NTNumber:
		result = node->value.intValue;
		break;
	case NTIdentifier:
		getValue (level->variableTable, node->value.strValue, &result);
		break;
	case NTMinus:
		result = 0 - evaluate (level, node->next);
		break;
	default:	/* an arithmetic binary operator. */
		value1 = evaluate (level, node->next);
		value2 = evaluate (level, node->next->next);
		switch (node->type) {
		case NTAdd:			result = value1+value2; break;
		case NTSubtract:	result = value1-value2; break;
		case NTMultiply:	result = value1*value2; break;
		case NTDivide:		result = value1/value2; break;
		} 
	} 
	return result;
}

void interpretVariableDeclaration(Level* level, treeNode* node)
{
	char* id = node->structure->value.strValue;
	treeNode* expression;

	if (!isDeclared (level->variableTable, id)) {
		addId(&level->variableTable, node->structure->value.strValue);
	}
	else {
		errorCount++;
		fprintf(stderr, "Error:\t Variable %s already declared.\n", id);
		return;
	}
	
	expression = node->structure->next;
	if (expression != NULL)
	{
		setValue(level->variableTable, id, evaluate(level, expression));
	}
}

void interpretAssignment(Level* level, treeNode* node)
{
	char* id = node->structure->value.strValue;
	if (isDeclared (level->variableTable, id)) {
		setValue(level->variableTable, id, evaluate(level, node->structure->next));
	} else {
		errorCount++;
		fprintf(stderr, "Error:\t Variable %s has not been declared.\n", id);
	}
}

void interpretDrawCommand(Level* level, treeNode* node)
{
	treeNode* command = node->structure;
	switch (command->type) {
		case NTDrawPoint: interpretDrawPoint(level, command); break;
		case NTDrawCircle: interpretDrawCircle(level, command); break;
		case NTDrawLine: interpretDrawLine(level, command); break;
		case NTDrawRect: interpretDrawRect(level, command); break;
		default:
			errorCount++;
			fprintf(stderr, "Compiler Error: Type %d is not a valid draw command.", command->type);
	}
}

void interpretDrawPoint(Level* level, treeNode* node)
{
	treeNode* symbolNode;
	int x, y;
	symbolNode = node->structure;
	x = evaluate(level, symbolNode->next->structure);
	y = evaluate(level, symbolNode->next->structure->next);
	
	if(isDeclared(level->symbolTable, symbolNode->value.strValue)) {
		int symbol;
		getValue(level->symbolTable, symbolNode->value.strValue, &symbol);
		plotSymbol(level, (char)symbol, x, y);
	}
	else
	{
		errorCount++;
		fprintf(stderr, "Error:\t%s is not a declared symbol", symbolNode->value.strValue);
	}

}

void interpretDrawCircle(Level* level, treeNode* node)
{
	treeNode* symbolNode;
	int x0, y0, radius;
	symbolNode = node->structure;
	x0 = evaluate(level, symbolNode->next->structure);
	y0 = evaluate(level, symbolNode->next->structure->next);
	radius = evaluate(level, symbolNode->next->next);
	
	if(isDeclared(level->symbolTable, symbolNode->value.strValue)) {
		int symbol;
		int x, y;
		getValue(level->symbolTable, symbolNode->value.strValue, &symbol);
		
		/* Brute force circle algorithm */
		for ( y = -radius; y <= radius; ++y) {
			for (x = -radius; x <= radius; ++x) {
				if (x*x + y*y <= radius*radius) {
					plotSymbol(level, (char)symbol, x0 + x, y0 + y);
				}
			}
		}
	}
	else
	{
		errorCount++;
		fprintf(stderr, "Error:\t%s is not a declared symbol", symbolNode->value.strValue);
	}
}

void interpretDrawLine(Level* level, treeNode* node)
{
	treeNode* symbolNode;
	int x0, x1, y0, y1;
	symbolNode = node->structure;
	x0 = evaluate(level, symbolNode->next->structure);
	y0 = evaluate(level, symbolNode->next->structure->next);
	x1 = evaluate(level, symbolNode->next->next->structure);
	y1 = evaluate(level, symbolNode->next->next->structure->next);
	
	if(isDeclared(level->symbolTable, symbolNode->value.strValue)) {
		int symbol;
		int x = x0, y = y0;
		int i;
		int dx = abs(x1 - x0);
		int dy = abs(y1 - y0);
		int sx = sign(x1 - x0);
		int sy = sign(y1 - y0);
		int swap = 0;
		int D = 0;
		getValue(level->symbolTable, symbolNode->value.strValue, &symbol);
		
		/* Generalized Bresenham's Line Algorithm */
		if (dy > dx) { int temp = dx; dx = dy; dy = temp; swap = 1; }
		D = 2*dy - dx;
		for ( i = 0; i < dx; ++i) {
			plotSymbol(level, (char)symbol, x, y);
			while ( D >= 0 ) {
				D = D - 2*dx;
				if (swap) x+=sx;
				else y+=sy;
			}
			D = D + 2*dy;
			if (swap) y+=sy;
			else x+=sx;
		}
		
	}
	else
	{
		errorCount++;
		fprintf(stderr, "Error:\t%s is not a declared symbol", symbolNode->value.strValue);
	}
}

void interpretDrawRect(Level* level, treeNode* node)
{
	treeNode* symbolNode;
	int x0, x1, y0, y1;
	symbolNode = node->structure;
	x0 = evaluate(level, symbolNode->next->structure);
	y0 = evaluate(level, symbolNode->next->structure->next);
	x1 = evaluate(level, symbolNode->next->next->structure);
	y1 = evaluate(level, symbolNode->next->next->structure->next);
	
	if(isDeclared(level->symbolTable, symbolNode->value.strValue)) {
		int symbol;
		int left, right;
		int top, bottom;
		int x, y;
		getValue(level->symbolTable, symbolNode->value.strValue, &symbol);
		
		if (x1 > x0) {
			left = x0;
			right = x1;
		} else {
			right = x0;
			left = x1;
		}
		
		if (y1 > y0) {
			top = y0;
			bottom = y1;
		} else {
			bottom = y0;
			top = y1;
		}
		
		for (x = left; x <= right; ++x) {
			for (y = top; y <= bottom; ++y) {
				plotSymbol(level, (char)symbol, x, y);
			}
		}
	}
}

/* Where x = 0 is the far left of the grid, and y = 0 is the top of the grid */
void plotSymbol(Level *level, char symbol, int x, int y)
{
	if (x >= level->width) {
		if (flags & FLAG_PRINT_WARNINGS) 
			fprintf (stderr, "Warning: attempting to plot point at (%d, %d) which is not on the grid \"%s\".\n", x, y, level->name);
		return;
	}
	if (x < 0) {
		if (flags & FLAG_PRINT_WARNINGS) 
			fprintf (stderr, "Warning: attempting to plot point at (%d, %d) which is not on the grid \"%s\".\n", x, y, level->name);
		return;
	}
	if (y >= level->height) {
		if (flags & FLAG_PRINT_WARNINGS) 
			fprintf (stderr, "Warning: attempting to plot point at (%d, %d) which is not on the grid \"%s\".\n", x, y, level->name);
		return;
	}
	if (y < 0) {
		if (flags & FLAG_PRINT_WARNINGS) 
			fprintf (stderr, "Warning: attempting to plot point at (%d, %d) which is not on the grid \"%s\".\n", x, y, level->name);
		return;
	}
	level->tiles[x+(y*level->width)] = symbol;
}

void outputPlainText(LevelListNode* levelList)
{
	LevelListNode* it = levelList;
	size_t i, j;

	do {
		fprintf(outputFile, "Name\t: %s\n", it->this->name);
		fprintf(outputFile, "Size\t: %d\n", it->this->size);
		fprintf(outputFile, "Width\t: %d\n", it->this->width);
		fprintf(outputFile, "Height\t: %d\n", it->this->height);
		for (i = 0; i < it->this->size; ++i) {
			/* Output a new line if it's the end of a row */
			if (i%it->this->width == 0) fprintf(outputFile, "\n");
			/* Output the next character in the grid */
			fprintf(outputFile, "%c", it->this->tiles[i]);
		}
		fprintf(outputFile, "\n\n");
		
		if (flags & FLAG_DRAW_SYMBOL_TABLE)
		{
			printf("=== VARIABLES ===\n\n");
			drawSymbolTable(it->this->variableTable, 0);
			printf("=== SYMBOLS ===\n\n");
			drawSymbolTable(it->this->symbolTable, 1);
		}
		
		it = it->next;
	} while (it != NULL);
}

void outputXML(LevelListNode* levelList)
{
	LevelListNode* it = levelList;
	size_t i, j;

	do {
		fprintf(outputFile, "<level>\n");
		fprintf(outputFile, "\t<name>%s</name>\n", it->this->name);
		fprintf(outputFile, "\t<size>%d</size>\n", it->this->size);
		fprintf(outputFile, "\t<width>%d</width>\n", it->this->width);
		fprintf(outputFile, "\t<height>%d</height>\n", it->this->height);
		fprintf(outputFile, "\t<tiles>");
		for (i = 0; i < it->this->size; ++i) {
			/* Output a new line if it's the end of a row */
			if (i%it->this->width == 0) fprintf(outputFile, "\n\t");
			/* Output the next character in the grid */
			fprintf(outputFile, "%c", it->this->tiles[i]);
		}
		fprintf(outputFile, "\n\t</tiles>\n</level>\n\n");
		
		if (flags & FLAG_DRAW_SYMBOL_TABLE)
		{
			printf("=== VARIABLES ===\n\n");
			drawSymbolTable(it->this->variableTable, 0);
			printf("=== SYMBOLS ===\n\n");
			drawSymbolTable(it->this->symbolTable, 1);
		}
		
		it = it->next;
	} while (it != NULL);
}
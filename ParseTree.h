#ifndef PARSE_TREE_H
#define PARSE_TREE_H

enum NodeType {
	NTLevelDefinitionList, NTLevelDefinition, NTLevelInfo, NTLevelName,
	NTLevelSize, NTCoordinate, NTSymbolDefinitionList, 
	NTSymbolDefinition, NTStatementList, NTStatement, NTRepeatBlock, 
	NTVariableDeclaration, NTAssignment, NTNumber, NTString, NTChar, 
	NTIdentifier, NTAdd, NTSubtract, NTMultiply, NTDivide, NTMinus,
	NTExpression, NTDrawCommand, NTDrawPoint, NTDrawCircle, NTDrawLine,
	NTDrawRect
};

/* A node of the parse tree. */
typedef struct treeNode {
	int type;
	union data {
		int  intValue;	/* only for tNUMBER nodes. */
		char charValue; /* only for tCHARACTER nodes. */
		char *strValue;	/* for tSTRING and tIDENTIFIER nodes */
	} value;
	struct treeNode *structure; /* pointer to child node; start of sibling list representing the structure of this non-terminal. */
	struct treeNode *next; /* pointer to next sibling element of the production applied for the parent non-terminal. */
} treeNode;

#endif//! PARSE_TREE_H

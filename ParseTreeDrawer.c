/*
	Draws the parse tree to standard out.
	
	The format of the parse tree is as follows:
		
		node	next
		|
		structure
		
	Note:
		The 'next' nodes are drawn before the 'structure' nodes.
*/

#include "ParseTreeDrawer.h"

int tab;			/* The number of tabs the current node is at. */
int currentTab;		/* The number of tabs the cursor is at. */

char *getTypeName(int type);

/* Draws a parse tree. topNode is the top node of the tree, from which the rest 
   of the tree will be drawn from */
int drawParseTree(treeNode *topNode)
{
	tab = 0;
	currentTab = 0;

	printf("=== PARSE TREE ===\n\n");
	printf("%s", getTypeName(topNode->type));
	if (topNode->next != NULL)
	{
		tab++;
		currentTab++;
		draw(topNode->next);
	}
	tab = 0;
	currentTab = 0;
	
	if (topNode->structure != NULL)
	{
		printf("\n");
		printf("|\n");
		draw(topNode->structure);
	}
	printf("\n\n");
	
	return 0;
}

/* Draws a node. Then draws it's siblings and children recursivly. */
int draw(treeNode *node)
{
	int thisTab = tab;
	
	int i;
	for (i = currentTab; i < thisTab; ++i)
	{
		currentTab++;
		printf("\t");
	}
	
	printf("%s", getTypeName(node->type));
	
	if (node->next != NULL)
	{
		tab++;
		draw(node->next);
	}
	tab = thisTab;
	
	if (node->structure != NULL)
	{
		printf("\n");
		for (i = 0; i < thisTab; ++i)
			printf("\t");
		printf("|\n");
		currentTab = 0;		
		draw(node->structure);
	}
	
	return 0;
}

/* A function to convert the enumerated NodeType into understandable text.
   All return values must be strings of length less than 8 to remove confusion 
   with tabs */
char *getTypeName(int type)
{
	switch(type)
	{
		case NTLevelDefinitionList:
			return "LDefLs"; break;
		case NTLevelDefinition:
			return "LDef"; break;
		case NTLevelInfo:
			return "LInfo"; break;
		case NTLevelName:
			return "LName"; break;
		case NTLevelSize:
			return "LSize"; break;
		case NTCoordinate:
			return "Coord"; break;
		case NTSymbolDefinitionList:
			return "SDefLs"; break;
		case NTSymbolDefinition:
			return "SDef"; break;
		case NTStatementList:
			return "StmLs"; break;
		case NTStatement:
			return "Stmnt"; break;
		case NTRepeatBlock:
			return "Repeat"; break;
		case NTVariableDeclaration:
			return "VarDec"; break;
		case NTAssignment:
			return "Ass"; break;
		case NTNumber:
			return "Number"; break;
		case NTString:
			return "String"; break;
		case NTChar:
			return "Char"; break;
		case NTIdentifier:
			return "Ident"; break;
		case NTAdd:
			return "Add"; break;
		case NTSubtract:
			return "Sub"; break;
		case NTMultiply:
			return "Multi"; break;
		case NTDivide:
			return "Div"; break;
		case NTMinus:
			return "Minus"; break;
		case NTExpression:
			return "Expr"; break;
		case NTDrawCommand:
			return "DrawCm"; break;
		case NTDrawPoint:
			return "DrawP"; break;
		case NTDrawCircle:
			return "DrawCr"; break;
		case NTDrawLine:
			return "DrawLn"; break;
		default:
			return "ERROR";
	}
}
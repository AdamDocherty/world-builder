

%{
#include <stdlib.h>
#include <stdio.h>

/* include */

//#include "SymbolTable.h"
#include "ParseTree.h"
#include "ParseTreeBuilder.h"

extern int sourceLineNo;

/* count the number of lines detected. */
int errorCount = 0;

/* report a semnatic error. */
void semanticError (int, char*);

int yylex();
void yyerror(char*);

treeNode *parseTreeTopNode;

%}

%union {
	int number;
	char* string;
	char character;
	struct treeNode* parseTree;
}

/* non-terminals that return parse tree branches */
%type <parseTree> levelDefinitionList
%type <parseTree> levelDefinition
%type <parseTree> levelInfo
%type <parseTree> symbolDefinitionList
%type <parseTree> levelName
%type <parseTree> levelSize
%type <parseTree> coordinate
%type <parseTree> symbolDefinition
%type <parseTree> statementList
%type <parseTree> statement
%type <parseTree> variableDeclaration
%type <parseTree> drawCommand
%type <parseTree> repeatBlock
%type <parseTree> assignmentStatement
%type <parseTree> expression
%type <parseTree> drawPoint
%type <parseTree> drawCircle
%type <parseTree> drawLine
%type <parseTree> drawRect
%type <character> character

%token <number> tNUMBER 
%token <string> tIDENTIFIER 
%token <string> tSTRING_VALUE 
%token <character> tCHARACTER

%token tBEGIN_LEVEL tVARIABLE tNAME tSIZE tDEFINE tREPEAT tTIMES
%token tEND_REPEAT tDRAW tCIRCLE tCENTRE tRADIUS tLINE tRECT tFROM tTO tEND_LEVEL

%left '-' '+'
%left '*' '/'
%left NEG 	/* for unary minus. */

%%

worldBuilderScript :
	levelDefinitionList { if (errorCount == 0) parseTreeTopNode = $1; }
	;

levelDefinitionList :
	levelDefinition { $$ = newLevelDefinitionList(NULL, $1); }
	| levelDefinitionList levelDefinition { $$ = newLevelDefinitionList($1, $2); }
	;

levelDefinition	:
	tBEGIN_LEVEL levelInfo symbolDefinitionList statementList tEND_LEVEL { $$ = newLevelDefinition( $2, $3, $4 ); }
	;

levelInfo	:
	levelName levelSize { $$ = newLevelInfo( $1, $2 ); }
	;

levelName	:
	tNAME '=' tSTRING_VALUE { $$ = newLevelName( $3 ); }
	;

levelSize	:
	tSIZE '=' coordinate { $$ = newLevelSize( $3 ); }
	;

coordinate	:
	'(' expression ',' expression ')' { $$ = newCoordinate( $2, $4 ); }
	;

symbolDefinitionList	:
	symbolDefinition { $$ = newSymbolDefinitionList(NULL, $1); }
	| symbolDefinitionList symbolDefinition { $$ = newSymbolDefinitionList($1, $2); }
	;

symbolDefinition	:
	tDEFINE	tIDENTIFIER character { $$ = newSymbolDefinition( $2, $3 ); }
	;

character	:
	tIDENTIFIER	{ 
				if(strlen($1) > 1) 
				{ 
					printf("Line %d: Symbols must be a single character only.\n", 
					sourceLineNo);
					YYERROR;
				}
				else
				{
					$$ = yylval.string[0];
				}
			}
	| tCHARACTER { $$ = yylval.string[0]; }

statementList	:
	statement { $$ = newStatementList(NULL, $1); }
	| statementList statement { $$ = newStatementList($1, $2); }
	;

statement	:
	variableDeclaration { $$ = newStatement( $1 ); }
	| drawCommand { $$ = newStatement( $1 ); }
	| repeatBlock { $$ = newStatement( $1 ); }
	| assignmentStatement { $$ = newStatement( $1 ); }
	;

variableDeclaration	:
	tVARIABLE tIDENTIFIER { $$ = newVariableDeclaration( $2, NULL ); }
	| tVARIABLE tIDENTIFIER '=' expression { $$ = newVariableDeclaration( $2, $4 ); }
	;

assignmentStatement	:
	tIDENTIFIER '=' expression { $$ = newAssignment( $1, $3 ); }
	;

expression	:
	tNUMBER { $$ = newNumExpr( $1 ); }
	| tIDENTIFIER { $$ = newIdExpr ( $1 ); }
	| expression '+' expression { $$ = newExpression (NTAdd, $1, $3); }
	| expression '-' expression { $$ = newExpression (NTSubtract, $1, $3); }
	| expression '*' expression { $$ = newExpression (NTMultiply, $1, $3); }
	| expression '/' expression { $$ = newExpression (NTDivide, $1, $3); }
	| '-' expression %prec NEG  { $$ = newUnaryMinusExpr ($2); }
	| '(' expression ')' { $$ = $2; }
	;

drawCommand	:
	drawPoint { $$ = newDrawCommand( $1 ); }
	| drawCircle { $$ = newDrawCommand( $1 ); }
	| drawLine { $$ = newDrawCommand( $1 ); }
	| drawRect { $$ = newDrawCommand( $1 ); }
	;

drawPoint :
	tDRAW tIDENTIFIER coordinate { $$ = newDrawPoint( $2, $3 ); }
	;
	
drawCircle	:
	tDRAW tIDENTIFIER tCIRCLE '{' tCENTRE '=' coordinate ',' tRADIUS '=' expression '}' { $$ = newDrawCircle( $2, $7, $11 ); }
	;

drawLine	:
	tDRAW tIDENTIFIER tLINE '{' tFROM '=' coordinate ',' tTO '=' coordinate '}' { $$ = newDrawLine( $2, $7, $11 ); }
	;
	
drawRect	:
	tDRAW tIDENTIFIER tRECT '{' tFROM '=' coordinate ',' tTO '=' coordinate '}' { $$ = newDrawRect( $2, $7, $11 ); }
	;

repeatBlock	:
	tREPEAT expression tTIMES statementList tEND_REPEAT { $$ = newRepeatBlock( $2, $4 ); }
	;

%%

void yyerror (char* msg) {
	fprintf(stderr, "line %d: %s\n", sourceLineNo, msg);
	errorCount++;
}


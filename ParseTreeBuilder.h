#ifndef PARSE_TREE_BUILDER_H
#define PARSE_TREE_BUILDER_H

#include "ParseTree.h"

/* Allocate space for a parse tree node and initialise fields. */
treeNode* newNode(int nodetype);

/* List may be null if the levelDefinition production is being applied */
treeNode* newLevelDefinitionList(treeNode *list, treeNode *levelDefinition);

treeNode* newLevelDefinition(treeNode *levelInfo, treeNode *symbolDefinitionList, treeNode *statementList);

treeNode* newLevelInfo(treeNode *levelName, treeNode *levelSize);

treeNode* newLevelName(char *name);

treeNode* newLevelSize(treeNode *coordinate);

treeNode* newCoordinate(treeNode *xExpr, treeNode *yExpr);

/* List may be null if the symbolDefinition production is being applied */
treeNode* newSymbolDefinitionList(treeNode *list, treeNode* symbolDefinition);

treeNode* newSymbolDefinition(char *id, char character);

/* List may be null if the statement production is being applied */
treeNode* newStatementList(treeNode* list, treeNode* statement);

treeNode* newStatement(treeNode* statement);

/* Assignment may be NULL if the variable is not initialised in the same statement 
   Id may be NULL if the assignmentStatement production is being applied */
treeNode* newVariableDeclaration(char* id, treeNode *assignment);

treeNode* newAssignment(char *id, treeNode *expr);

treeNode* newExpression(int operator, treeNode* exprLeft, treeNode* exprRight);

treeNode* newNumExpr(int number);

treeNode* newIdExpr(char *id);

treeNode* newUnaryMinusExpr(treeNode *expr);

treeNode* newDrawCommand(treeNode* drawCommand);

treeNode* newDrawPoint(char* id, treeNode *coord);

treeNode* newDrawCircle(char* id, treeNode *centreCoord, treeNode *radiusExpr);

treeNode* newDrawLine(char* id, treeNode *fromCoord, treeNode *toCoord);

treeNode* newDrawRect(char* id, treeNode *fromCoord, treeNode *toCoord);

treeNode* newRepeatBlock(treeNode *specifier, treeNode *statementList);

treeNode* newCharacter(char character);

treeNode* newIdentifier(char* id);

treeNode* newNumber(int number);



#endif//! PARSE_TREE_BUILDER_H

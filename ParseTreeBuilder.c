/*
	Builds the parse tree from parameters given by the parser.
*/

#include <stdlib.h>

#include "ParseTree.h"
#include "ParseTreeBuilder.h"

treeNode* newNode(int nodeType)
{
	treeNode *node = (treeNode*)malloc (sizeof(treeNode));
	memset ((void*)node, 0, sizeof(treeNode));
	node->type = nodeType;
	return node;
}

treeNode* newLevelDefinitionList(treeNode* list, treeNode *levelDefinition)
{
	treeNode *node = newNode(NTLevelDefinitionList);
	if (list != NULL)
	{
		node->structure = list;
		list->next = levelDefinition;
	}
	else
	{
		node->structure = levelDefinition;
	}
	return node;
}

treeNode* newLevelDefinition(treeNode *levelInfo, treeNode *symbolDefinitionList, treeNode *statementList)
{
	treeNode *node = newNode(NTLevelDefinition);
	node->structure = levelInfo;
	levelInfo->next = symbolDefinitionList;
	symbolDefinitionList->next = statementList;
	return node;
}

treeNode* newLevelInfo(treeNode *levelName, treeNode *levelSize)
{
	treeNode *node = newNode(NTLevelInfo);
	node->structure = levelName;
	levelName->next = levelSize;
	return node;
}

treeNode* newLevelName(char* name)
{
	treeNode *node = newNode(NTLevelName);
	node->value.strValue = name;
	return node;
}

treeNode* newLevelSize(treeNode *coordinate)
{
	treeNode *node = newNode(NTLevelSize);
	node->structure = coordinate;
	return node;
}

treeNode* newCoordinate(treeNode *xExpr, treeNode *yExpr)
{
	treeNode *node = newNode(NTCoordinate);
	node->structure = xExpr;
	xExpr->next = yExpr;
	return node;
}

treeNode* newSymbolDefinitionList(treeNode *list, treeNode *symbolDefinition)
{
	treeNode *node = newNode(NTSymbolDefinitionList);
	if (list != NULL)
	{
		node->structure = list;
		list->next = symbolDefinition;
	}
	else
	{
		node->structure = symbolDefinition;
	}
	return node;
}

treeNode* newSymbolDefinition(char* id, char symbol)
{
	treeNode* node = newNode(NTSymbolDefinition);
	treeNode* idNode = newIdentifier(id);
	node->structure = idNode;
	node->value.charValue = symbol;
	return node;
}

treeNode* newStatementList(treeNode* list, treeNode* statement)
{
	treeNode *node = newNode(NTStatementList);
	if (list != NULL)
	{
		node->structure = list;
		list->next = statement;
	}
	else
	{
		node->structure = statement;
	}
	return node;
}

treeNode* newStatement(treeNode* statement)
{
	treeNode *node = newNode(NTStatement);
	node->structure = statement;
	return node;
}

treeNode* newVariableDeclaration(char* id, treeNode *expression)
{
	treeNode *node = newNode(NTVariableDeclaration);
	treeNode *idNode = newIdentifier(id);
	node->structure = idNode;
	if (expression != NULL)
	{
		idNode->next = expression;
	}
	
	return node;
}

treeNode* newAssignment(char* id, treeNode *expr)
{
	treeNode *node = newNode(NTAssignment);
	treeNode *idNode = newIdentifier(id);
	node->structure = idNode;
	idNode->next = expr;
	return node;
}

treeNode* newExpression(int operator, treeNode *exprLeft, treeNode *exprRight)
{
	treeNode *node = newNode(NTExpression);
	treeNode *opNode = newNode(operator);
	node->structure = opNode;
	opNode->next = exprLeft;
	exprLeft->next = exprRight;
	return node;
}

treeNode* newNumExpr(int number)
{
	treeNode *node = newNode (NTExpression);
	node->structure = newNumber (number);
	return node;
}

treeNode* newIdExpr(char *id)
{
	treeNode *node = newNode (NTExpression);
	node->structure = newIdentifier (id);
	return node;
}

treeNode* newUnaryMinusExpr(treeNode *expr)
{
	treeNode *node = newNode (NTIdentifier);
	treeNode *opNode = newNode(NTMinus);
	node->structure = opNode;
	opNode->next = expr;
	return node;
}

treeNode* newDrawCommand(treeNode* drawCommand)
{
	treeNode *node = newNode (NTDrawCommand);
	node->structure = drawCommand;
	return node;
}

treeNode* newDrawPoint(char* id, treeNode *coord)
{
	treeNode *node = newNode (NTDrawPoint);
	treeNode *idNode = newIdentifier (id);
	node->structure = idNode;
	idNode->next = coord;
	return node;
}

treeNode* newDrawCircle(char* id, treeNode *centreCoord, treeNode *radiusExpr)
{
	treeNode *node = newNode(NTDrawCircle);
	treeNode *idNode = newIdentifier (id);
	node->structure = idNode;
	idNode->next = centreCoord;
	centreCoord->next = radiusExpr;
	return node;
}

treeNode* newDrawLine(char* id, treeNode *fromCoord, treeNode *toCoord)
{
	treeNode *node = newNode(NTDrawLine);
	treeNode *idNode = newIdentifier (id);
	node->structure = idNode;
	idNode->next = fromCoord;
	fromCoord->next = toCoord;
	return node;
}

treeNode* newDrawRect(char* id, treeNode *fromCoord, treeNode *toCoord)
{
	treeNode *node = newNode(NTDrawRect);
	treeNode *idNode = newIdentifier (id);
	node->structure = idNode;
	idNode->next = fromCoord;
	fromCoord->next = toCoord;
	return node;
}

treeNode* newRepeatBlock(treeNode *specifier, treeNode *statementList)
{
	treeNode *node = newNode(NTRepeatBlock);
	node->structure = specifier;
	specifier->next = statementList;
	return node;
}

treeNode* newCharacter(char character)
{
	treeNode *node = newNode(NTChar);
	node->value.charValue = character;
	return node;
}

treeNode* newIdentifier(char* id)
{
	treeNode *node = newNode(NTIdentifier);
	node->value.strValue = id;
	return node;
}
	
treeNode* newNumber(int number)
{
	treeNode *node = newNode(NTNumber);
	node->value.intValue = number;
	return node;
}
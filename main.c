/*
	Entry point for the program.
	Handles loading in the files and setting the flags variable.
	
	(c) Adam Docherty, November 2013
*/

#include "ParseTree.h"
#include "ParseTreeDrawer.h"
#include "ParseTreeInterpreter.h"
#include "SymbolTable.h"
#include "SymbolTableDrawer.h"
#include "WorldBuilder.h"

#include <stdlib.h>
#include <stdio.h>

/* number of errors detected - defined in GenVal.y */
extern int errorCount;

/* source of input script - defined in Lex. */
extern FILE *yyin;

/* output file for generated dataset. */
FILE *outputFile;

/* the top node of the parse tree */
extern treeNode *parseTreeTopNode;

/* Any flags passed in */
int flags = 0;

/* The index of the input file and output file in argv */
int inFileArg = 0;
int outFileArg = 0;

/* internal function prototypes. */
int prologue (int, char**);		/* return 0 if invalid usage. */
int handleFlag(char);			/* return 0 if flag is invalid */
void printHelp();
void epilogue ();
int openFiles (int, char**);		/* return 0 if error opening files. */
void closeFiles ();

/* the Yacc parser prototype. */
int yyparse();

int main(int argc, char *argv[])
{
	if (!prologue (argc, argv)) { exit (-1); }
	
	if (flags & FLAG_HELP)
	{
		printHelp();
	}
	else // If the help flag is not set
	{
		if (!openFiles (argc, argv)) { exit (-2); }

		yyparse();
		
		if (!(flags & FLAG_CANCEL_INTERPRETER)) {
			interpret(parseTreeTopNode);
		}
		
		if (flags & FLAG_DRAW_PARSE_TREE)
			drawParseTree(parseTreeTopNode);
		
		closeFiles (argc);
		epilogue();
	}
	return 0;
}

/* Display prologue information and check correct usage. */
int prologue (int argc, char *argv[]) {
	fprintf (stderr, "\n\n=== WorldBuilder Compiler. \n");
	fprintf (stderr, "(c) Adam Docherty, November 2013. \n\n");
	if (argc<2) {
		fprintf (stderr, "invalid usage: WorldBuilder <flags> <script-file> {<output-file>}\n");
		return 0;
	} else {
		int i;
		for (i = 1; i < argc; ++i)
		{
			if(inFileArg == 0)
			{
				/* Check for flags before the input file name has been found */
				char *c = argv[i];
				if(*c++ == '-') {
					for (;*c != '\0'; ++c) {
						if (!handleFlag(*c)) {
							return 0;
						}
						if (flags & FLAG_HELP) return 1; // Exit out if the help flag has been set.
					}
				}
				else
				{
					inFileArg = i;
				}
			}
			else if(outFileArg == 0)
			{
				/* Test if the user is attempting to use any flags after the file name */
				if(!strncmp(argv[i], "-", 1)) {
					fprintf (stderr, "invalid usage: WorldBuilder <flags> <script-file> {<output-file>}\n");
					return 0;
				}
				outFileArg = i;
			}
			else /* The user has already entered the input and output file names */
			{
				fprintf (stderr, "invalid usage: WorldBuilder <flags> <script-file> {<output-file>}\n");
				return 0;
			}
		}
		/* Output an error if the input file name was not set */
		if (inFileArg == 0) {
			fprintf (stderr, "invalid usage: WorldBuilder <flags> <script-file> {<output-file>}\n");
			return 0;
		}
	
		fprintf (stderr, "Compiling WorldBuilder script '%s'. \n", argv[inFileArg]);
		fprintf (stderr, "Level output to ");
		if (outFileArg > 0)
				fprintf (stderr, "file '%s'. \n\n", argv[outFileArg]);
		else 	fprintf (stderr, "standard output. \n\n");
	}
	return 1;
}

int handleFlag(char c)
{
	switch(c)
	{
	case 'p':
		flags |= FLAG_DRAW_PARSE_TREE;
		return 1;
	case 's':
		flags |= FLAG_DRAW_SYMBOL_TABLE;
		return 1;
	case 'h':
		flags |= FLAG_HELP;
		return 1;
	case 'x':
		flags |= FLAG_OUTPUT_XML;
		return 1;
	case 'w':
		flags |= FLAG_PRINT_WARNINGS;
		return 1;
	case 'C':
		flags |= FLAG_CANCEL_INTERPRETER;
	default:
		fprintf (stderr, "-%c is not a valid flag. To see all flags, run the program with the flag -h\n", c);
		return 0;
	}
}

void printHelp()
{
	fprintf (stderr, "\tHow to use WorldBuilder \n\n");
	fprintf (stderr, "\tFlags\n");
	fprintf (stderr, "\t-w\tPrint warnings.\n");
	fprintf (stderr, "\t-x\tOutput in XML.\n");
	fprintf (stderr, "\t-p\tPrint the parse tree.\n");
	fprintf (stderr, "\t-s\tPrint the symbol tables.\n");
	fprintf (stderr, "\t-C\tDisable the interpreter.\n");
}

/* open and close files depending on command-line arguments. */
/* Return 0 (false) if any error found opening the files. */
int openFiles (int argc, char *argv[]) {
	if (fopen_s (&yyin, argv[inFileArg], "r")) {
		fprintf (stderr, "Error opening WorldBuilder script '%s'.", argv[inFileArg]);
		return 0;
	}
	if (outFileArg > 0) {
		if (fopen_s (&outputFile, argv[outFileArg], "w")) {
			fprintf (stderr, "Error opening file '%s' for writing generated map data.", argv[outFileArg]);
			return 0;
		}
	}
	return 1;
}

void epilogue () {
	if (errorCount == 0)
		fprintf (stderr, "\n\nMap successfully created. \n");
	else	fprintf (stderr, "\n%d errors found; no dataset created. \n", errorCount);
}

void closeFiles () {
	fclose (yyin);
	if (outFileArg > 0) fclose (outputFile);
}
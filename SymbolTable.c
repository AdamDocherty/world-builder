/**
	Symbol Table class, adapted from Allan Milne's Symbol Table class.
	Adapted to support multiple symbol tables
	
	(c) Adam Docherty, November 2013
*/

#include <stdlib.h>

/* include the interface function prototypes. */
#include "SymbolTable.h"

/* local function to find an entry for the specified id name; returns null if not found. */
symbolNode *findEntry (symbolNode* symbolTable, char *id) {
	symbolNode *found = symbolTable;
	while (found!=NULL && strcmp(id,found->name)) {
		found = found->next;
	}
	return found;
}

/* returns true/false if there is an entry in the table for the specified id name. */
int isDeclared (symbolNode* symbolTable, char *id) {
	return (findEntry(symbolTable, id) != NULL);
}

/* adds a new entry for the specified id name; returns false if already in the table. */
int addId (symbolNode** symbolTable, char *id) {
	symbolNode *newEntry;
	if (isDeclared (*symbolTable, id)) return 0;
	newEntry = (symbolNode*) malloc (sizeof(symbolNode));
	newEntry->name = id;
	newEntry->value = 0;
	newEntry->next = *symbolTable;
	*symbolTable = newEntry;
	return 1;
}

/* sets the v parameter to the value associated in the table with the specified id name; returns false if no table entry exists. */
int getValue (symbolNode* symbolTable, char *id, int *v) {
	symbolNode *entry = findEntry (symbolTable, id);
	if (entry == NULL) return 0;
	*v = entry->value;
	return 1;
}

/* sets the value associated with the specified id name to v; returns false if no table entry exists. */
int setValue (symbolNode* symbolTable, char *id, int v) {
	symbolNode *entry = findEntry (symbolTable, id);
	if (entry == NULL) return 0;
	entry->value = v;
	return 1;
}
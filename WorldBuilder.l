/*
	WorldBuilder Compiler
	Lex specification for the WorldBuilder language for use with the compiler version.
	(c) Adam Docherty, November 2012.

	Notes on this version:
	1. Returns both the token types and any associated semantic value.
	2. The results are case sensitive
	3. The language ignores white space.
*/

/* Comment this line out when working on older version of Flex (Uni Computers) */
%option nounistd

DIGIT		[0-9]

/* Declare two exclusive start conditions for C and C++ style comments */
%x		C_COMMENT
%x		CPP_COMMENT

%{
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include "WorldBuilder.tab.h"

void yyerror(char*);
void lexError(char);

int sourceLineNo = 1;
%}

%%

	/* keyword tokens. */
(beginlevel)	{ return tBEGIN_LEVEL; }
(var)		{ return tVARIABLE; }
(name)		{ return tNAME; }
(size)		{ return tSIZE; }
(define)	{ return tDEFINE; }
(repeat)	{ return tREPEAT; }
(times)		{ return tTIMES; }
(endrepeat)	{ return tEND_REPEAT; }
(draw)		{ return tDRAW; }
(circle)	{ return tCIRCLE; }
     /* support both the american spelling and the correct spelling of centre. */
(centre)	{ return tCENTRE; }
(center)	{ return tCENTRE; }
(line)		{ return tLINE; }
(from)		{ return tFROM; }
(to)		{ return tTO; }
(rect)		{ return tRECT; }
(rectangle)	{ return tRECT; }
(endlevel)	{ return tEND_LEVEL; }
(radius)	{ return tRADIUS; }

	/* single character punctuation. */
[{}=\-+*/,()]	{
		return *yytext;
	}

	/* c-style comments */
"/*"		{ BEGIN(C_COMMENT); }

<C_COMMENT>"*/"	{ BEGIN(INITIAL); }

<C_COMMENT>\n	{ sourceLineNo++; }

<C_COMMENT>.	;
	/* c++-style comments */
"//"		{ BEGIN(CPP_COMMENT); }

<CPP_COMMENT>\n	{ sourceLineNo++;	BEGIN(INITIAL); }

<C_COMMENT>"//" ;

<CPP_COMMENT>.	;

	/* integer and real numbers. */
{DIGIT}+("."{DIGIT}+)?	{
	sscanf_s(yytext, "%d", &yylval.number);
	return tNUMBER;
	}

	/* identifier (multi-character string starting with a character) */
[a-zA-Z][a-zA-Z0-9]*	{
	yylval.string = (char*)malloc (yyleng+1);
		strcpy(yylval.string, yytext);
		return tIDENTIFIER;
	}

	/* A quoted promt string; no escape characters; end of line is an error. */
\"[^\n\"]*\n	{	
	lexError((char)0);
	sourceLineNo++;
	}

\"[^\n\"]*\"	{
	yylval.string = (char*)malloc(yyleng-1);
	strncpy(yylval.string, yytext + 1, yyleng-2); /* Do not copy the quotes into yylval */
	return tSTRING_VALUE;
	}
	/*" /* Skip whitespace and increment line number. */
[ \t]	;
\n	{
		sourceLineNo++;
	}

.	{ 
	yylval.string = (char*)malloc(yyleng);
	strcpy(yylval.string, yytext);
	return tCHARACTER; 
	}

%%

int yywrap() { return 1; }

void lexError(char ch) {
	char errmsg[200];
	if (ch == 0) {
		sprintf_s(errmsg, 200, "(Line %d) Premature end of prompt string.", 
		sourceLineNo);
	} else {
		sprintf_s (errmsg, 200, "(Line %d) Invalid character '%c' found.", 
		sourceLineNo, ch);
	}
	yyerror(errmsg);
}

